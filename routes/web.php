<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index')->name('index');

Route::get('/demo','UsuarioController@lista');

Route::resource('almacen','AlmacenController');
Route::get('/almacenes/lista','AlmacenController@index')->name('almacenes.lista');

Route::resource('producto','ProductoController');
Route::get('/productos/lista','ProductoController@index')->name('productos.lista');

Route::resource('usuario','UsuarioController');
Route::get('/usuarios/lista','UsuarioController@index')->name('usuarios.lista');

Route::resource('registroEntrada','RegistroEntradaController');
Route::get('/registroentradas/lista','RegistroEntradaController@index')->name('registroentradas.lista');

Route::resource('registroSalida','RegistroSalidaController');
Route::get('/registrosalidas/lista','RegistroSalidaController@index')->name('registrosalidas.lista');

Route::get('/kardex/lista','KardexController@index')->name('kardex.lista');

Route::resource('existencia','ExistenciaController');
Route::get('/inventario/lista','InventarioController@index')->name('inventario.lista');
Route::get('/inventario/{almacen}','InventarioController@detalle');
Route::get('/inventario/exportar/{almacen}','InventarioController@exportar');

Route::get('/kardex/exportar/{lista}','KardexController@exportar');

Route::resource('documento','DocumentoController');
Route::resource('disposicion','DisposicionController');

Route::get('/login','LoginController@index')->name('login');
Route::post('/loginprocess','LoginController@validar')->name('logueo');
Route::get('/logout','LoginController@logout')->name('logout');
Route::get('/loginprocess','LoginController@index');

Route::resource('ticket','TicketController');
Route::get('/tickets/lista','TicketController@index')->name('tickets.lista');
Route::get('/ticketNotificado/{idnotificacion}','TicketController@showTicketNotificado');
Route::put('/ticketNotificado/{idnotificacion}','TicketController@updateTicketNotificado');

Route::get('/acercade/mostrar','HomeController@mostrar')->name('acercade.mostrar');
#Auth::routes();
