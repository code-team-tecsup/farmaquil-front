@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Productos
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('ProductoController@update',$producto->idproducto)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <fieldset>

                        <legend class="sumario">Editar Producto</legend>
                         @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="txtCodigo">Código</label>
                            <input type="text" class="form-control" id="txtCodigo" name="txtCodigo"
                                   value="{{$producto->idproducto}}" placeholder="Ingrese un Codigo" readonly maxlength="10">
                            <div class="invalid-feedback">
                                Por favor ingrese un Código con 10 caracteres como máximo.
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtDescripcion">Descripcion</label>
                            <input type="text" class="form-control " id="txtDescripcion" name="txtDescripcion"
                                   value="{{$producto->descripcion}}" placeholder="Ingrese una Descripcion" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese una descripción con 50 caracteres como máximo.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-5">
                                <div class="form-group ">
                                    <label for="txtUnidad">Unidad</label>
                                    <select class="form-control btn-light " id="txtUnidad" name="txtUnidad" >
                                        @foreach($unidades as $unidad)
                                            @if($unidad->idunidad==$producto->idunidad)
                                                <option value={{$unidad->idunidad}} selected>{{$unidad->descripcion}} </option>
                                            @else
                                                <option value={{$unidad->idunidad}}>{{$unidad->descripcion}} </option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        Por favor seleccione la Unidad.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group ">
                                    <label for="txtPresentacion">Presentación</label>
                                    <input type="text" class="form-control " id="txtPresentacion" name="txtPresentacion"
                                           value="{{$producto->presentacion}}" placeholder="Ingrese la Presentación" required autofocus maxlength="50">
                                    <div class="invalid-feedback">
                                        Por favor ingrese una Presentación con 50 caracteres como máximo.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtTipo">Tipo</label>
                            <select class="form-control btn-light " id="txtTipo" name="txtTipo" >
                                @foreach($tipos as $tipo)
                                    @if($tipo->idtipo==$producto->idtipo)
                                        <option value={{$tipo->idtipo}} selected>{{$tipo->descripcion}} </option>
                                    @else
                                        <option value={{$tipo->idtipo}}>{{$tipo->descripcion}} </option>
                                    @endif
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Por favor seleccione un tipo.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-warning">Actualizar <i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("productos.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
