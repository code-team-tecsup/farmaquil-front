@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                MANTENIMIENTO DE PRODUCTOS
            </div>
            <div class="col-3">
                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <a class="btn btn-success" href="{{route('producto.create')}}">
                    Crear <i class="fas fa-file-alt"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">CODIGO</th>
                        <th scope="col">DESCRIPCION</th>
                        <th scope="col">PRESENTACION</th>
                        <th scope="col">UNIDAD</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($productos as $producto)
                        <tr>
                            <td>{{$producto->idproducto}}</td>
                            <td>{{$producto->descripcion}}</td>
                            <td>{{$producto->presentacion}}</td>
                            <td>{{$producto->unidadMedida->descripcion}}</td>
                            <td>{{$producto->tipo->descripcion}}</td>

                            <td>
                                <a class="btn btn-warning" href="{{action('ProductoController@edit',$producto->idproducto)}}">
                                    Editar <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a class="btn btn-danger" href="{{action('ProductoController@show',$producto->idproducto)}}">
                                    Eliminar <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="mx-auto">
                        {{$productos->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

