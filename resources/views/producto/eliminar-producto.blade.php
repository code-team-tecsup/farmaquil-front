@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Productos
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('ProductoController@destroy',$producto->idproducto)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('DELETE')
                    <fieldset>
                        <legend class="sumario">Eliminar Producto</legend>
                        @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="txtCodigo">Código</label>
                            <input type="text" class="form-control" id="txtCodigo" name="txtCodigo"
                                   value="{{$producto->idproducto}}" placeholder="Ingrese un Codigo" readonly >
                            <div class="invalid-feedback">
                                Por favor ingrese un Codigo
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtDescripcion">Descripcion</label>
                            <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
                                   value="{{$producto->descripcion}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-5">
                                <div class="form-group ">
                                    <label for="txtPresentacion">Unidad</label>
                                    <input type="text" class="form-control" id="txtUnidad" name="txtUnidad"
                                           value="{{$producto->unidadMedida->descripcion}}" readonly   >
                                    <div class="invalid-feedback">
                                        Por favor seleccione una unidad.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="form-group ">
                                    <label for="txtPresentacion">Presentación</label>
                                    <input type="text" class="form-control" id="txtPresentacion" name="txtPresentacion"
                                           value="{{$producto->presentacion}}" readonly   >
                                    <div class="invalid-feedback">
                                        Por favor ingrese la presentación.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtTipo">Tipo</label>
                            <input type="text" class="form-control" id="txtTipo" name="txtTipo"
                                   value="{{$producto->tipo->descripcion}}" readonly   >

                            <div class="invalid-feedback">
                                Por favor seleccione un tipo.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-danger">Eliminar <i class="fas fa-trash-alt iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("productos.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
