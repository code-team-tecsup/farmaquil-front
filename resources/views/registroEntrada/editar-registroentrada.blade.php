@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Ingreso
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-12 col-sm-12">
                <form class="needs-validation"  action="" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <fieldset>

                        <legend class="sumario">Detalle Ingreso</legend>

                        <div class="form-group margenes-input col-sm-5 float-left">
                            <label for="txtTipo">Tipo</label>
                            <input type="text" class="form-control" id="txtTipo" name="txtTipo"
                                   value="{{$registroentradas->Almacen->Tipo->descripcion}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-md-5 float-lg-left">
                            <label for="txtAlmacen">Almacen</label>
                            <input type="text" class="form-control" id="txtAlmacen" name="txtAlmacen"
                                   value="{{$registroentradas->Almacen->descripcion}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-sm-2 float-left">
                            <label for="txtOperacion">Operación</label>
                            <input type="text" class="form-control" id="txtOperacion" name="txtOperacion"
                                   value="1234" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-sm-3 float-left">
                            <label for="txtFecha">F. Ingreso</label>
                            <input type="text" class="form-control" id="txtFecha" name="txtFecha"
                                   value="{{$registroentradas->fecha}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-sm-3 float-left">
                            <label for="txtDocumento">Documento</label>
                            <input type="text" class="form-control" id="txtDocumento" name="txtDocumento"
                                   value="{{$registroentradas->Documento->descripcion}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-sm-2 float-left">
                            <label for="txtSerie">Serie</label>
                            <input type="text" class="form-control" id="txtSerie" name="txtSerie"
                                   value="{{$registroentradas->serie}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group margenes-input col-sm-2 float-left">
                            <label for="txtNumero">Número</label>
                            <input type="text" class="form-control" id="txtNumero" name="txtNumero"
                                   value="{{$registroentradas->numero}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">CÓDIGO</th>
                                <th scope="col">DESCRIPCIÓN</th>
                                <th scope="col">DISPOSICIÓN</th>
                                <th scope="col">CANTIDAD</th>
                                <th scope="col">LOTE</th>
                                <th scope="col">VENCIMIENTO</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($registroentradas->detalleRegistroEntradas as $detalles)
                                <tr>
                                    <td>{{$detalles->Producto->idproducto}}</td>
                                    <td>{{$detalles->Producto->descripcion}}</td>
                                    <td>{{$detalles->Disposicion->descripcion}}</td>
                                    <td>{{$detalles->cantidad}}</td>
                                    <td>{{$detalles->lote}}</td>
                                    <td>{{$detalles->vencimiento}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ redirect()->back()->getTargetUrl() }}'">Retornar <i class="fas fa-undo-alt iconoBoton"></i></button>

                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
