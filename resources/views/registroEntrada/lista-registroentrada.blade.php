@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                Listado de Ingresos
            </div>
            <div class="col-3">
                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <a class="btn btn-success" href="{{route('registroEntrada.create')}}">
                    Crear <i class="fas fa-file-alt"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">F. INGRESO</th>
                        <th scope="col">TIPOS</th>
                        <th scope="col">ALMACEN</th>
                        <th scope="col">SERIE</th>
                        <th scope="col">NÚMERO</th>
                        <th scope="col">USUARIO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($registroentradas as $registroentrada)
                        <tr>
                            <td>{{$registroentrada->fecha}}</td>
                            <td>{{$registroentrada->Almacen->Tipo->descripcion}}</td>
                            <td>{{$registroentrada->Almacen->descripcion}}</td>
                            <td>{{$registroentrada->serie}}</td>
                            <td>{{$registroentrada->numero}}</td>
                            <td>{{$registroentrada->Usuario->nombres}}</td>

                            <td>

                                <a class="btn btn-warning" href="{{action('RegistroEntradaController@edit',$registroentrada->identrada)}}">
                                    Ver <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a class="btn btn-danger" href="{{action('RegistroEntradaController@show',$registroentrada->identrada)}}">
                                    Eliminar <i class="fas fa-trash-alt"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
