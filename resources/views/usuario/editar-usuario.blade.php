@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Usuarios
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('UsuarioController@update',$usuario->idusuario)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <fieldset>
                        <legend class="sumario">Editar Usuario</legend>
                        @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group ">
                            <label for="txtApellidos">Apellidos</label>
                            <input type="text" class="form-control " id="txtApellidos" name="txtApellidos"
                                   value="{{$usuario->apellidos}}" placeholder="Ingrese los apellidos" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese los apellidos con 50 caracteres como máximo.
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtNombres">Nombres</label>
                            <input type="text" class="form-control " id="txtNombres" name="txtNombres"
                                   value="{{$usuario->nombres}}" placeholder="Ingrese los nombres" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese los nombres con 50 caracteres como máximo.
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtUsuario">Usuario</label>
                            <input type="text" class="form-control" id="txtUsuario" name="txtUsuario"
                                   value="{{$usuario->usuario}} " placeholder="Ingrese el nombre de usuario" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese el nombre de usuario con 50 caracteres como máximo.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtPassword">Contraseña</label>
                                    <input type="password" class="form-control" id="txtPassword" name="txtPassword" placeholder="Ingrese su contraseña." pattern=".{6,}" required autofocus >
                                    <div class="invalid-feedback">
                                        Por favor ingrese su nueva contraseña con 6 caracteres como mínimo.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtPasswordConfirmado">Confirme su Contraseña</label>
                                    <input type="password" class="form-control" id="txtPasswordConfirmado" name="txtPasswordConfirmado" placeholder="Confirme su contraseña." pattern=".{6,}" required autofocus >
                                    <div class="invalid-feedback">
                                        Por favor confirme su nueva contraseña con 6 caracteres como mínimo.
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtRol">Rol</label>
                                    <select class="form-control btn-light" id="txtRol" name="txtRol" >
                                        @foreach($roles as $rol)
                                            @if($rol->idrol==$usuario->idrol)
                                                <option value={{$rol->idrol}} selected>{{$rol->descripcion}} </option>
                                            @else
                                                <option value={{$rol->idrol}}>{{$rol->descripcion}} </option>
                                            @endif
                                        @endforeach

                                    </select>
                                    <div class="invalid-feedback">
                                        Por favor seleccione un rol.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtActivo">Activo</label>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input position-static" id="txtActivo" name="txtActivo" {{$usuario->activo ? 'checked' : '' }}   >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-warning">Actualizar <i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("usuarios.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
