@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                MANTENIMIENTO DE USUARIOS
            </div>
            <div class="col-3">
                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <a class="btn btn-success" href="{{action('UsuarioController@create')}}">
                    Crear <i class="fas fa-file-alt"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">APELLIDOS</th>
                        <th scope="col">NOMBRES</th>
                        <th scope="col">USUARIO</th>
                        <th scope="col">ROL</th>
                        <th scope="col">ACTIVO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $usuario)
                        <tr>
                            <td>{{$usuario->apellidos}}</td>
                            <td>{{$usuario->nombres}}</td>
                            <td>{{$usuario->usuario}}</td>
                            <td>{{$usuario->rol->descripcion}}</td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="checkbox" {{$usuario->activo ? 'checked' : '' }} disabled >
                                </div>
                            </td>



                            <td>
                                <a class="btn btn-warning" href="{{action('UsuarioController@edit',$usuario->idusuario)}}">
                                    Editar <i class="fas fa-pencil-alt iconoBoton"></i>
                                </a>

                                <a class="btn btn-danger" href="{{action('UsuarioController@show',$usuario->idusuario)}}">
                                    Eliminar <i class="fas fa-trash-alt IconColorRojo"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="mx-auto">
                        {{$usuarios->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
