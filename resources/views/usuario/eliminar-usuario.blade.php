@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Usuarios
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{ action('UsuarioController@destroy',$usuario->idusuario)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('DELETE')
                    <fieldset>
                        <legend class="sumario">Eliminar Usuario</legend>

                        <div class="form-group margenes-input">
                            <label for="txtApellidos">Apellidos</label>
                            <input type="text" class="form-control" id="txtApellidos" name="txtApellidos"
                                   value="{{$usuario->apellidos}}" readonly >
                            <div class="invalid-feedback">
                                Por favor ingrese los apellidos.
                            </div>
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtNombres">Nombres</label>
                            <input type="text" class="form-control" id="txtNombres" name="txtNombres"
                                   value="{{$usuario->nombres}}" readonly >
                            <div class="invalid-feedback">
                                Por favor ingrese los nombres.
                            </div>
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtUsuario">Usuario</label>
                            <input type="text" class="form-control" id="txtUsuario" name="txtUsuario"
                                   value="{{$usuario->usuario}}" readonly >
                            <div class="invalid-feedback">
                                Por favor ingrese el nombre de usuario.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtRol">Rol</label>
                                    <input type="text" class="form-control" id="txtTipo" name="txtTipo"
                                           value="{{$usuario->rol->descripcion}}" readonly   >
                                    <div class="invalid-feedback">
                                        Por favor seleccione un rol.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group ">
                                    <label for="txtActivo">Activo</label>
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input position-static" id="txtActivo" name="txtActivo" {{$usuario->activo ? 'checked' : '' }} disabled  >
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-danger">Eliminar <i class="fas fa-trash-alt IconColorRojo"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("usuarios.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
