@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Ticket
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('TicketController@update',$ticket->idticket)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    {{ method_field('PUT') }}
                    <fieldset>
                        <legend class="sumario">Editar Ticket</legend>
                    @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group margenes-input">
                            <label for="txtFecha">Fecha</label>
                            <input type="text" class="form-control" id="txtFecha" name="txtFecha" readonly
                                   value= "{{$ticket->fecha}}" >
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtTitulo">Titulo</label>
                            <input type="text" class="form-control" id="txtTitulo" name="txtTitulo"  placeholder="Ingrese un Titulo"
                                   value="{{$ticket->titulo}}" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese un Titulo.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <label for="txtPrioridad">Prioridad</label>
                                <select class="form-control btn-light" id="txtPrioridad" name="txtPrioridad" >
                                    @foreach($prioridades as $prioridad)
                                        @if($prioridad->idprioridad==$ticket->idprioridad)
                                            <option value={{$prioridad->idprioridad}} selected>{{$prioridad->descripcion}} </option>
                                        @else
                                            <option value={{$prioridad->idprioridad}}>{{$prioridad->descripcion}} </option>
                                        @endif
                                    @endforeach

                                </select>
                                <div class="invalid-feedback">
                                    Por favor seleccione una prioridad.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="txtUsuario">Usuario</label>
                                <select class="form-control btn-light" id="txtUsuario" name="txtUsuario" >
                                    @foreach($usuarios as $usuario)
                                        @if($usuario->idusuario==$ticket->idusuarioOrigen)
                                            <option value={{$usuario->idusuario}} selected>{{$usuario->nombres}} </option>
                                        @else
                                            <option value={{$usuario->idusuario}}>{{$usuario->nombres}} </option>
                                        @endif
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Por favor seleccione un usuario.
                                </div>
                            </div>
                        </div>



                        <div class="form-group margenes-input">
                            <label for="txtContenido">Contenido</label>
                            <textarea  class="form-control" id="txtContenido" name="txtContenido"  rows="1"  placeholder="Ingrese un Contenido" required autofocus >
                                {{$ticket->contenido}}
                            </textarea>
                            <div class="invalid-feedback">
                                Por favor ingrese un Contenido.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-warning">Actualizar<i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("tickets.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
