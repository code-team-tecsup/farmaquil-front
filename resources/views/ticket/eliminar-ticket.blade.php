@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Ticket
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('TicketController@destroy',$ticket->idticket)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    {{ method_field('DELETE') }}
                    <fieldset>
                        <legend class="sumario">Eliminar Ticket</legend>
                    @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group margenes-input">
                            <label for="txtFecha">Fecha</label>
                            <input type="text" class="form-control" id="txtFecha" name="txtFecha" readonly
                                   value= "{{$ticket->fecha}}" >
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtTitulo">Titulo</label>
                            <input type="text" class="form-control" id="txtTitulo" name="txtTitulo"  placeholder="Ingrese un Titulo"
                                   value="{{$ticket->titulo}}" readonly>
                            <div class="invalid-feedback">
                                Por favor ingrese un Titulo.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <div class="form-group margenes-input">
                                    <label for="txtTitulo">Prioridad</label>
                                    <input type="text" class="form-control" id="txtTitulo" name="txtTitulo"  placeholder="Ingrese un Titulo"
                                           value="{{$ticket->prioridad->descripcion}}" readonly>
                                    <div class="invalid-feedback">
                                        Por favor ingrese una Prioridad
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group margenes-input">
                                    <label for="txtUsuario">Usuario</label>
                                    <input type="text" class="form-control" id="txtUsuario" name="txtUsuario"  placeholder="Ingrese un Usuario"
                                           value="{{$ticket->usuarioDestino->nombres}}" readonly>

                                    <div class="invalid-feedback">
                                        Por favor ingrese una Prioridad
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="form-group margenes-input">
                            <label for="txtContenido">Contenido</label>
                            <textarea  class="form-control" id="txtContenido" name="txtContenido"  rows="2" cols="50" readonly >
                                {{$ticket->contenido}}
                            </textarea>
                            <div class="invalid-feedback">
                                Por favor ingrese un Contenido.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-danger">Eliminar<i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("tickets.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
