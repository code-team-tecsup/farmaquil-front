@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                Listado de Tickets
            </div>

            <div class="col-3">
                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <a class="btn btn-success" href="{{route('ticket.create')}}">
                    Crear <i class="fas fa-file-alt"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead thead-light">
                    <tr>
                        <th scope="col">FECHA</th>
                        <th scope="col">TITULO</th>
                        <th scope="col">PRIORIDAD</th>
                        <th scope="col">ATENDIDO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($tickets as $ticket)
                        <tr>
                            <td>{{$ticket->fecha}}</td>
                            <td>{{$ticket->titulo}}</td>
                            <td>{{$ticket->prioridad->descripcion}}</td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input position-static" type="checkbox" {{$ticket->atendido ? 'checked' : '' }} disabled >
                                </div>
                            </td>

                            <td>
                                <!-- SI EL TICKET ESTA ATENDIDO NO SE MUESTRA -->
                                @if($ticket->atendido==false)
                                    <a class="btn btn-warning" href="{{action('TicketController@edit',$ticket->idticket)}}">
                                        Editar <i class="fas fa-pencil-alt iconoBoton"></i>
                                    </a>

                                    <a class="btn btn-danger" href="{{action('TicketController@show',$ticket->idticket)}}">
                                        Eliminar <i class="fas fa-trash-alt IconColorRojo"></i>
                                    </a>
                                @endif


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="mx-auto">
                        {{$tickets->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
