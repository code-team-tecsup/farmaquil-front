@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                CREAR TICKET
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('TicketController@store')}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    <fieldset>
                        <legend class="sumario">Nuevo Ticket</legend>

                        <div class="form-group margenes-input">
                            <label for="txtFecha">Fecha</label>
                            <input type="date" class="form-control" id="txtFecha" name="txtFecha" readonly
                                   value="<?=date('Y-m-d', time());?>"  >
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtTitulo">Titulo</label>
                            <input type="text" class="form-control" id="txtTitulo" name="txtTitulo"  placeholder="Ingrese un Titulo" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese un Titulo.
                            </div>
                        </div>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-md-6">
                                <label for="txtPrioridad">Prioridad</label>
                                <select class="form-control btn-light" id="txtPrioridad" name="txtPrioridad" >
                                    @foreach($prioridades as $prioridad)
                                        <option value={{$prioridad->idprioridad}}>{{$prioridad->descripcion}} </option>
                                    @endforeach

                                </select>
                                <div class="invalid-feedback">
                                    Por favor seleccione una prioridad.
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="txtUsuario">Usuario</label>
                                <select class="form-control btn-light" id="txtUsuario" name="txtUsuario" >
                                    @foreach($usuarios as $usuario)
                                        <option value={{$usuario->idusuario}}>{{$usuario->nombres}} </option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    Por favor seleccione un usuario.
                                </div>
                            </div>
                        </div>



                        <div class="form-group margenes-input">
                            <label for="txtContenido">Contenido</label>
                            <textarea  class="form-control" id="txtContenido" name="txtContenido"  rows="1"  placeholder="Ingrese un Contenido" required autofocus >

                            </textarea>
                            <div class="invalid-feedback">
                                Por favor ingrese un Contenido.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-success">Registrar <i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("tickets.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
