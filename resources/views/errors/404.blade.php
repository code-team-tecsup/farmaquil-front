@extends('layouts.errores')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
               UPSS LO SENTIMOS
            </div>
            <div class="col-3">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
            </div>
            <div class="col-6">
                {{"El recurso no esta disponible"}}

                <a class="dropdown-item" href="{{route('index')}}" >
                    <span class="sesion ">
                        {{__('Retornar')}} <i class="fas fa-home icono-otroColor"></i>
                    </span>
                </a>
            </div>
            <div class="col-3">
            </div>
        </div>
    </div>
@endsection

