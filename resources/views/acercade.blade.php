@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 titulo">
                Desarrollado Por:
            </div>
            <div class="col-4">
                <h5>Aisa Incapuño, Sixto Santiago</h5>
                <h5>Barreda Huanca, Kevin Steve</h5>
                <h5>Champi Huamani, Brayan Oscar</h5>
                <h5>Delgado Tejada, Jared Lehi</h5>
                <h5>Pinto Huaman, Kevin Alexander</h5>
                <h5>Soto Adco, Brandon Aldair Brayan</h5>
            </div>

            <div class="col-12 subtitulo">
                Proyecto final - Diseño y desarrollo de Software
            </div>

            <div class="col-12 subtitulo">
                Tecsup 2020
            </div>
        </div>
    </div>
@endsection
