<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">

    @livewireStyles
</head>
<body>
    <div id="app">
        <div class="cabecera cabecera-detalles"> <div class="container-fluid col-12">
                <div class="row justify-content-center align-items-center primera-fila-detalles">
                    <div class="col-3">
                        <img src="{{ asset('img/farmaquil.png') }}" class="rounded">
                    </div>
                    <div class="col-6 titulo">

                    </div>

                    <div class="col-3">
                        <div class="row justify-content-center align-items-center">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @yield('scripts')
    @livewireScripts
</body>
</html>
