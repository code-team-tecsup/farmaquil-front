<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('img/favicon.png') }}">

    @livewireStyles
</head>
<body>
    <div id="app">
        <div class="cabecera cabecera-detalles"> <div class="container-fluid col-12">
                <div class="row justify-content-center align-items-center primera-fila-detalles">
                    <div class="col-3">

                        <a class="nav-link" href="{{ route('index') }}">
                            <img src="{{ asset('img/farmaquil.png') }}" class="rounded">
                        </a>


                    </div>
                    <div class="col-6 titulo">
                        {{__('KARDEX AUTOMATIZADO')}}
                    </div>

                    <div class="col-3">
                        <div class="row justify-content-center align-items-center">
                            <div class="col">
                                <span class="float-right sesion">
                                     <i class="fas fa-user-alt icono"></i>
                                    {{Auth::user()->rol->descripcion}} | {{Auth::user()->usuario }}
                                </span>
                            </div>
                        </div>

                        <?php $listaTicket=Auth::user()->ticketPrioridad() ?>
                        <?php $ticketSinLeer=count($listaTicket) ?>

                        <div class="row justify-content-center align-items-center">
                            <div class="col-6">
                                <li class="nav-item dropdown active">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbar?" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="sesion ">

                                            @if($ticketSinLeer>0)
                                                <i class="fas fa-bell icono"></i>
                                            @else
                                                <i class="fas fa-bell-slash icono"></i>
                                            @endif


                                            {{'Tickets'}}
                                        </span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <!-- SE REALIZA UN BUCLE PARA MOSTRAR LAS NOTIFICAIONES SIN LEER -->


                                        @foreach ($listaTicket as $ticket)
                                        <!-- SE LLAMA AL METODO SHOWPOSTNOTIFICADO PARA MARCAR COMO LEIDA LA NOTIFICACION -->
                                            <a class="dropdown-item"  href="{{action('TicketController@showTicketNotificado',$ticket['idticket'])}}"  >
                                                <!-- SE MUESTRA EL NOMBRE DEL USUARIO Y EL TITULO DEL POST -->
                                                <i>{{ $ticket['titulo'] }}</i> solicita
                                                <b>{{ $ticket['usuarioOrigen'] }}</b>
                                            </a>
                                        @endforeach
                                    </div>
                                </li>
                            </div>
                            <div class="col-6">
                                <a class="dropdown-item" href="{{action('LoginController@logout')}}" >
                                   <span class="float-right sesion ">
                                    {{__('Salir')}} <i class="fas fa-sign-out-alt icono-otroColor"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid primera-fila-detalles">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark navbar-fixed-top">
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#miNavBar"> <i class="fas fa-bars iconoBoton"></i> </button>
                    <div class="collapse navbar-collapse" id="miNavBar">
                        <ul class="navbar-nav ">
                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarArchivo" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{__('Archivo')}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarArchivo">

                                    @if(Auth::user()->hasRole('ALMACEN'))
                                        <a class="dropdown-item" href="{{ route('almacenes.lista') }}">{{__('Almacen')}}</a>
                                        <div class="dropdown-divider"></div>

                                        <a class="dropdown-item" href="{{ route('productos.lista') }}">{{__('Producto')}}</a>
                                        <div class="dropdown-divider"></div>
                                    @endif

                                    @if(Auth::user()->hasRole('ADMINISTRADOR'))
                                        <a class="dropdown-item" href="{{ route('usuarios.lista') }}">{{__('Usuario')}}</a>
                                        <div class="dropdown-divider"></div>
                                    @endif

                                    <a class="dropdown-item" href="{{ route('tickets.lista') }}">{{__('Tickets')}}</a>

                                </div>
                            </li>
                            <li class="nav-item dropdown active">
                                @if(Auth::user()->hasRole('ALMACEN'))
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarProceso" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{__('Proceso')}}
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarProceso">
                                        <a class="dropdown-item" href="{{ route('registroentradas.lista') }}">{{__('Ingreso de Productos')}}</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="{{ route('registrosalidas.lista') }}">{{__('Salida de Productos')}}</a>
                                    </div>
                                @endif
                            </li>
                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarReporte" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{__('Reportes')}}
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarReporte">
                                    <a class="dropdown-item" href="{{route('inventario.lista')}}">Inventario</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ route('kardex.lista') }}">{{__('Kardex')}}</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="#" id="navbar?" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    ?
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbar?">
                                    <a class="dropdown-item" href="{{route('acercade.mostrar')}}">Acerca de...</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>


        <main class="py-4">
            @yield('content')
        </main>
    </div>
    @yield('scripts')

    @livewireScripts
</body>
</html>
