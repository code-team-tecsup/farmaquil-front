@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Almacenes
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('AlmacenController@store')}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    <fieldset>
                        <legend class="sumario">Nuevo Almacen</legend>
                        @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group margenes-input">
                            <label for="txtDescripcion">Descripcion</label>
                            <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"  placeholder="Ingrese una Descripcion" required autofocus maxlength="50">
                            <div class="invalid-feedback">
                                Por favor ingrese una Descripcion con 50 caracteres como máximo.
                            </div>
                        </div>

                        <div class="form-group margenes-input">
                            <label for="txtTipo">Tipo</label>
                            <select class="form-control btn-light" id="txtTipo" name="txtTipo" >
                                @foreach($tipos as $tipo)
                                    <option value={{$tipo->idtipo}}>{{$tipo->descripcion}} </option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Por favor seleccione un tipo.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-success">Registrar <i class="fas fa-save iconoBoton"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("almacenes.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
