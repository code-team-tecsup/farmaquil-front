@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Mantenimiento de Almacenes
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-md-2">
            </div>

            <div class="col-md-6 col-sm-12">
                <form class="needs-validation"  action="{{action('AlmacenController@destroy',$almacen->idalmacen)}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    @method('DELETE')
                    <fieldset>
                        <legend class="sumario">Eliminar Almacen</legend>
                        @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group ">
                            <label for="txtDescripcion">Descripcion</label>
                            <input type="text" class="form-control" id="txtDescripcion" name="txtDescripcion"
                                   value="{{$almacen->descripcion}}" readonly   >
                            <div class="invalid-feedback">
                                Por favor ingrese una descripcion.
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="txtTipo">Tipo</label>
                            <input type="text" class="form-control" id="txtTipo" name="txtTipo"
                                   value="{{$almacen->tipo->descripcion}}" readonly   >

                            <div class="invalid-feedback">
                                Por favor seleccione un tipo.
                            </div>
                        </div>

                        <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                        <button type="submit" class="btn btn-danger">Eliminar <i class="fas fa-trash-alt IconColorRojo"></i></button>

                        <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("almacenes.lista") }}'">Cancelar <i class="fas fa-undo-alt iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="col-md-2">
            </div>
        </div>
    </div>


@endsection
