@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                MANTENIMIENTO DE ALMACENES
            </div>

            <div class="col-3">
                    <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                    <a class="btn btn-success" href="{{route('almacen.create')}}">
                        Crear <i class="fas fa-file-alt"></i>
                    </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead thead-light">
                        <tr>
                            <th scope="col">DESCRIPCION</th>
                            <th scope="col">TIPO</th>
                            <th scope="col">ACCIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($almacenes as $almacen)
                            <tr>
                                <td>{{$almacen->descripcion}}</td>
                                <td>{{$almacen->tipo->descripcion}}</td>

                                <td>
                                    <a class="btn btn-warning" href="{{action('AlmacenController@edit',$almacen->idalmacen)}}">
                                        Editar <i class="fas fa-pencil-alt"></i>
                                    </a>

                                    <a class="btn btn-danger" href="{{action('AlmacenController@show',$almacen->idalmacen)}}">
                                       Eliminar <i class="fas fa-trash-alt"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="row">
                    <div class="mx-auto">
                        {{$almacenes->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
