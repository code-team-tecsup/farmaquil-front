@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                Listado de Salidas
            </div>
            <div class="col-3">
                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <a class="btn btn-success" href="{{route('registroSalida.create')}}">
                    Crear <i class="fas fa-file-alt"></i>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">F. SALIDA</th>
                        <th scope="col">TIPOS</th>
                        <th scope="col">ALMACEN</th>
                        <th scope="col">SERIE</th>
                        <th scope="col">NÚMERO</th>
                        <th scope="col">USUARIO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($registrosalidas as $registrosalida)
                        <tr>
                            <td>{{$registrosalida->fecha}}</td>
                            <td>{{$registrosalida->almacen->tipo->descripcion}}</td>
                            <td>{{$registrosalida->almacen->descripcion}}</td>
                            <td>{{$registrosalida->serie}}</td>
                            <td>{{$registrosalida->numero}}</td>
                            <td>{{$registrosalida->usuario->nombres}}</td>

                            <td>
                                <a class="btn btn-warning" href="{{action('RegistroSalidaController@edit',$registrosalida->idsalida)}}">
                                    Ver <i class="fas fa-pencil-alt"></i>
                                </a>

                                <a class="btn btn-danger" href="{{action('RegistroSalidaController@show',$registrosalida->idsalida)}}">
                                    Eliminar <i class="fas fa-trash-alt"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

