@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 subtitulo">
                Salida
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12">
                @livewire('crear-salida')
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
    </script>
@endsection

