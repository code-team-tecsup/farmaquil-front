@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3">
            </div>
            <div class="col-6 subtitulo Titulos">
                INVENTARIO {{ $almacen->descripcion  }}
            </div>

            <div class="col-3">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead thead-light">
                    <tr>
                        <th scope="col">CODIGO</th>
                        <th scope="col">MED</th>
                        <th scope="col">DESCRIPCION</th>
                        <th scope="col">STOCK</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($existencias as $existencia)
                        <tr>
                            <td>{{$existencia->producto->idproducto}}</td>
                            <td>{{$existencia->producto->unidadMedida->descripcion}}</td>
                            <td>{{$existencia->producto->descripcion}}</td>
                            <td>{{$existencia->stock}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
                <button type="button" class="btn btn-warning" onclick="window.location='{{action("InventarioController@exportar",$almacen->idalmacen)}}'">Exportar <i class="fas fa-file-excel icono"></i></button>

                <button type="button" class="btn btn-secondary float-right" onclick="window.location='{{ route("inventario.lista") }}'">Retornar <i class="fas fa-undo-alt iconoBoton"></i></button>


            </div>
        </div>
    </div>
@endsection

