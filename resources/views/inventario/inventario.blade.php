@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3">
            </div>
            <div class="col-6 subtitulo Titulos">
                LISTADO DE ALMACENES
            </div>

            <div class="col-3">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <table class="table">
                    <thead class="thead thead-light">
                    <tr>
                        <th scope="col">TIPO</th>
                        <th scope="col">DESCRIPCION</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($almacenes as $almacen)
                        <tr>
                            <td>{{$almacen->tipo->descripcion}}</td>
                            <td>{{$almacen->descripcion}}</td>
                            <td>
                                <a class="btn btn-secondary" href="{{action('InventarioController@detalle',$almacen->idalmacen)}}">
                                   Ver Detalle  <i class="fas fa-pencil-alt iconoBoton"></i>
                                </a>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

