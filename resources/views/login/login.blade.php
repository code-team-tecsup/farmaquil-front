@extends('layouts.credenciales')

@section('content')

    <div class="container-fluid">
        <div class="row justify-content-center align-items-center">
            <div class="col-3"></div>
            <div class="col-6 subtitulo Titulos">
                LOGIN
            </div>
            <div class="col-3">
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
            </div>
            <div class="col-6">
                <form class="needs-validation" action="{{action('LoginController@validar')}}" method="post" enctype="multipart/form-data" novalidate>
                    @csrf
                    <fieldset>
                        <legend class="sumario">Acceso Autorizado</legend>
                        @if(count($errors)>0)
                        <!-- SI HUBIERA ERRORES SE MUESTRA UNA LISTA CON LOS ERRORES ENCONTRADOS   -->
                            <div class="alert alert-danger">
                                <ul><!--SE REALIZA UN BUCLE PARA MOSTRAR LOS ERRORES -->
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li><!-- SE MUESTRA EL ERROR  -->
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="txtUsuario"><i class="fas fa-user-alt icono"></i>  Usuario</label>
                            <input type="text" class="form-control" id="txtUsuario" name="txtUsuario"
                                   placeholder="Ingrese su nombre de usuario."  value="{{old('txtUsuario')}}" required autofocus>
                            <div class="invalid-feedback">
                                Por favor ingrese un nombre de usuario.
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="txtClave"><i class="fas fa-key icono"></i>  Contraseña</label>
                            <input type="password" class="form-control" id="txtClave" name="txtClave"
                                   placeholder="Ingrese su contraseña." required>
                            <div class="invalid-feedback">
                                Por favor ingrese su contraseña.
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success">Login <i class="fas fa-sign-in-alt     iconoBoton"></i></button>
                        <button type="reset" class="btn btn-secondary float-right" >Borrar <i class="fas fa-eraser iconoBoton"></i></button>
                    </fieldset>
                </form>
            </div>
            <div class="col-3">
            </div>
        </div>

        <div class="row">
            <div class="col-3">
            </div>
            <div class="col-6">
                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">USUARIO</th>
                        <th scope="col">CONTRASEÑA</th>
                        <th scope="col">ROL</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{"administrador"}}</td>
                            <td>{{"x64GKY"}}</td>
                            <td>{{"ADMINISTRADOR"}}</td>
                        </tr>
                        <tr>
                            <td>{{"almacen"}}</td>
                            <td>{{"x64GKY"}}</td>
                            <td>{{"ALMACEN"}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-3">
            </div>
        </div>

    </div>
@endsection

