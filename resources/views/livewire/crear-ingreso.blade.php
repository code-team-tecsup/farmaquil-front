<div>

    <form class="needs-validation" wire:submit.prevent="store" method="post"
          enctype="multipart/form-data" novalidate>
        @csrf
        <fieldset>
            <legend class="sumario">Nuevo Ingreso</legend>


            <div class="row justify-content-center align-items-center">
                <div class="col-6">
                    <div class="form-group ">
                        <label for="txtTipo">Tipo</label>
                        <select class="form-control btn-light" id="txtTipo" name="txtTipo" wire:change="filtro($event.target.value)"
                                wire:model="idtipo" {{$activo ? ' ':' disabled'}} required  >
                            @foreach($listaTipo as $tipo)
                                <option value={{$tipo->idtipo}}>{{$tipo->descripcion}} </option>
                            @endforeach
                        </select>
                        @error('idtipo') <span class="alert-danger">{{ "Seleccione un Tipo" }}</span> @enderror

                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group ">
                        <label for="txtAlmacen">Almacen</label>
                        <select class="form-control btn-light" id="txtAlmacen" name="txtAlmacen" wire:model="idalmacen"
                                {{$activo ? ' ':' disabled'}} required>
                            @foreach($listaAlmacen as $almacen)
                                <option value={{$almacen->idalmacen}}>{{$almacen->descripcion}} </option>
                            @endforeach
                        </select>
                        @error('idalmacen') <span class="alert-danger">{{ "Seleccione un Almacen" }}</span> @enderror

                    </div>
                </div>
            </div>

            <div class="row justify-content-center align-items-center">
                <div class="col-3">
                    <div class="form-group ">
                        <label for="txtFecha">F. Ingreso</label>
                        <input type="date" class="form-control" id="txtFecha" name="txtFecha" required readonly
                               min="<?=date('Y-m-d', time());?>"  wire:model="fecha" >
                        @error('date') <span class="alert-danger">{{ "Ingrese una Fecha" }}</span> @enderror

                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group ">
                        <label for="txtDocumento">Documento</label>
                        <select class="form-control btn-light" id="txtDocumento" name="txtDocumento" wire:model="iddocumento" required>
                            @foreach($listaDocumento as $documento)
                                <option value={{$documento->iddocumento}}>{{$documento->descripcion}} </option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="col-2">
                    <div class="form-group ">
                        <label for="txtSerie">Serie</label>
                        <input type="text" class="form-control" id="txtSerie" name="txtSerie" wire:model="serie"
                               placeholder="Ingrese una serie" maxlength="4" required autofocus>
                        @error('serie') <span class="alert-danger">{{ "Ingrese una Serie" }}</span> @enderror
                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group ">
                        <label for="txtNumero">Número</label>
                        <input type="text" class="form-control" id="txtNumero" name="txtNumero" wire:model="numero"
                               placeholder="Ingrese un número" maxlength="8" required >
                        @error('numero') <span class="alert-danger">{{ "Ingrese un Numero" }}</span> @enderror
                    </div>
                </div>

                <div class="col-2">
                </div>
            </div>

            <hr class="my-0 dividerClass"/>

            <div class="row justify-content-center align-items-center">
                <div class="col-3">
                    <div class="form-group ">
                        <label for="txtProducto">Producto</label>
                        <select class="form-control btn-light" id="txtProducto" name="txtProducto" wire:model="idproducto" required>
                            @foreach($listaProducto as $producto)
                                <option value={{$producto->idproducto}}>{{$producto->descripcion}} </option>
                            @endforeach
                        </select>

                    </div>
                </div>

                <div class="col-3">
                    <div class="form-group ">
                        <label for="txtDisposicion">Disposicion</label>
                        <select class="form-control btn-light" id="txtDisposicion" name="txtDisposicion" wire:model="iddisposicion" required>
                            @foreach($listaDisposicion as $disposicion)
                                <option value={{$disposicion->iddisposicion}}>{{$disposicion->descripcion}} </option>
                            @endforeach
                        </select>

                    </div>
                </div>

                <div class="col-1">
                    <div class="form-group ">
                        <label for="txtCantidad">Cantidad</label>

                        <input type="number" class="form-control" id="txtCantidad"
                               name="txtCantidad" placeholder="" wire:model="cantidad"  min="1">
                        @error('cantidad') <span class="alert-danger">{{ "Requerido" }}</span> @enderror


                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group ">
                        <label for="txtLote">Lote</label>
                        <input type="text" class="form-control" id="txtLote" name="txtLote"
                               placeholder="Ingrese el número de Lote" wire:model="lote" maxlength="20">
                        @error('lote') <span class="alert-danger">{{ "Ingrese un lote" }}</span> @enderror

                    </div>
                </div>

                <div class="col-2">
                    <div class="form-group ">
                        <label for="txtVencimiento">Vencimiento</label>
                        <select class="form-control btn-light" id="txtVencimiento" name="txtVencimiento" wire:model="idperiodo">
                            @foreach($listaPeriodo as $periodo)
                                <option value={{$periodo->idperiodo}}>{{$periodo->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-1">
                    <button type="button" class="btn btn-success float-right" wire:click="agregarDetalle()" >
                        Agregar
                    </button>
                </div>
            </div>

            @error('listaDetalle') <span class="alert-danger">{{ "Debe Agregar Productos al Detalle" }}</span> @enderror
            <div class="col-12">
                <table class="table" id="products_table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">CÓDIGO</th>
                        <th scope="col">DESCRIPCIÓN</th>
                        <th scope="col">DISPOSICIÓN</th>
                        <th scope="col">CANTIDAD</th>
                        <th scope="col">LOTE</th>
                        <th scope="col">VENCIMIENTO</th>
                        <th scope="col">ACCIONES</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($listaDetalle as $item)
                            <tr>
                                <td>{{ $item['idproducto'] }} </td>
                                <td>{{ $item['descripcionProducto'] }} </td>
                                <td>{{ $item['descripcionDisposicion'] }} </td>
                                <td>{{ $item['cantidad'] }} </td>
                                <td>{{ $item['lote'] }} </td>
                                <td>{{ $item['vencimiento'] }} </td>
                                <td>
                                    <button type="button" class="btn btn-danger" wire:click="eliminarDetalle({{ $item['iddetalle'] }})">
                                        Eliminar <i class="fas fa-undo-alt iconoBoton"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <!-- SI EL ROL TIENE PERMISOS PARA EDITAR -->
            <button type="submit" class="btn btn-success">Registrar <i class="fas fa-save iconoBoton"></i>
            </button>
            <button type="button" class="btn btn-secondary float-right"
                    onclick="window.location='{{ route("registroentradas.lista") }}'">Cancelar <i
                    class="fas fa-undo-alt iconoBoton"></i></button>

        </fieldset>
    </form>



</div>
