<div>

    <form class="needs-validation" wire:submit.prevent="store" method="post"
          enctype="multipart/form-data" novalidate>
        @csrf
        <fieldset>
            <legend class="sumario">KARDEX</legend>


            <div class="row justify-content-center align-items-center">
                <div class="col-6">
                    <div class="form-group ">
                        <label for="txtTipo">Tipo</label>
                        <select class="form-control btn-light" id="txtTipo" name="txtTipo"
                                wire:change="filtro($event.target.value)"
                                wire:model="idtipo" {{$activo ? ' ':' disabled'}} required>
                            @foreach($listaTipo as $tipo)
                                <option value={{$tipo->idtipo}}>{{$tipo->descripcion}} </option>
                            @endforeach
                        </select>
                        @error('idtipo') <span class="alert-danger">{{ "Seleccione un Tipo" }}</span> @enderror
                    </div>
                </div>

                <div class="col-6">
                    <div class="form-group ">
                        <label for="txtAlmacen">Almacen</label>
                        <select class="form-control btn-light" id="txtAlmacen" name="txtAlmacen" wire:model="idalmacen"
                                {{$activo ? ' ':' disabled'}} required>
                            @foreach($listaAlmacen as $almacen)
                                <option value={{$almacen->idalmacen}}>{{$almacen->descripcion}} </option>
                            @endforeach
                        </select>
                        @error('idalmacen') <span class="alert-danger">{{ "Seleccione un Almacen" }}</span> @enderror

                    </div>
                </div>
            </div>

            <div class="row justify-content-center align-items-center">
                <div class="col-6">
                    <div class="form-group ">
                        <label for="txtProducto">Producto</label>
                        <select class="form-control btn-light" id="txtProducto" name="txtProducto"
                                wire:model="idproducto" {{$activo ? ' ':' disabled'}}  required>
                            @foreach($listaProducto as $producto)
                                <option value={{$producto->idproducto}}>{{$producto->descripcion}} </option>
                            @endforeach
                        </select>

                    </div>
                </div>


                <div class="col-4">
                    <div class="form-group ">
                        <label for="txtVencimiento">Periodo</label>
                        <select class="form-control btn-light" id="txtVencimiento" name="txtVencimiento"
                                wire:model="idperiodo" {{$activo ? ' ':' disabled'}}   required>
                            @foreach($listaPeriodo as $periodo)
                                <option value={{$periodo->idperiodo}}>{{$periodo->descripcion}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-1">
                    <div class="form-group ">
                        <button type="button" class="btn btn-secondary float-right" wire:click="procesoKardex()" {{$activo ? ' ':' disabled'}} >
                            Procesar
                        </button>
                        @error('procesar') <span class="alert-danger">{{ "Proceso incorrecto" }}</span> @enderror
                    </div>
                </div>

                <div class="col-1">
                    <div class="form-group ">
                        <button type="button" class="btn btn-secondary float-right" wire:click="limpiarKardex()">
                           Limpiar
                        </button>
                    </div>
                </div>

            </div>

            @error('listaDetalle') <span class="alert-danger">{{ "Debe Agregar Productos al Detalle" }}</span> @enderror
            <div class="col-12">
                <table class="table" id="products_table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">ITEM</th>
                        <th scope="col">TIPO</th>
                        <th scope="col">FECHA</th>
                        <th scope="col">SERIE</th>
                        <th scope="col">NÚMERO</th>
                        <th scope="col">VENCIMIENTO</th>
                        <th scope="col">ENTRADA</th>
                        <th scope="col">SALIDA</th>
                        <th scope="col">SALDO</th>
                        <th scope="col">ACCIÓN</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listaDetalle as $item)
                        <tr  {{$item['tipo']=='ENTRADA' ? 'class=fila-entrada' : ' '}}>
                            <td>{{ $item['item'] }} </td>
                            <td>{{ $item['tipo'] }} </td>
                            <td>{{ $item['fecha'] }} </td>
                            <td>{{ $item['serie'] }} </td>
                            <td>{{ $item['numero'] }} </td>
                            <td>{{ $item['vencimiento'] }} </td>
                            <td>{{ $item['entrada'] }} </td>
                            <td>{{ $item['salida'] }} </td>
                            <td>{{ $item['saldo'] }} </td>
                            <td>

                                <!-- SI EL DETALLE NO TIENE MOVIMIENTOS -->
                                @if($item['id']!=0)
                                    <button type="button" class="btn btn-secondary"
                                            wire:click="verDetalle({{ $item['item'] }})">
                                        Ver <i class="fas fa-undo-alt iconoBoton"></i>
                                    </button>
                                @endif


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </fieldset>
    </form>


</div>
