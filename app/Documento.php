<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Documento extends Model
{
    protected $primaryKey = 'iddocumento';
}
