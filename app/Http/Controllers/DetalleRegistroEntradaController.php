<?php

namespace App\Http\Controllers;

use App\DetalleRegistroEntrada;
use App\Producto;
use App\RegistroEntrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DetalleRegistroEntradaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detalleRegistroEntrada = DetalleRegistroEntrada::all();
        return view('',compact('detalleRegistroEntrada'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($idalmacen)
    {
        $listaIngresos = RegistroEntrada::all()->where('idalmacen',$idalmacen);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleRegistroEntrada  $detalleRegistroEntrada
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleRegistroEntrada $detalleRegistroEntrada)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleRegistroEntrada  $detalleRegistroEntrada
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleRegistroEntrada $detalleRegistroEntrada)
    {
        $listaIngresos = RegistroEntrada::all()->where('idalmacen');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleRegistroEntrada  $detalleRegistroEntrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleRegistroEntrada $detalleRegistroEntrada)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleRegistroEntrada  $detalleRegistroEntrada
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleRegistroEntrada $detalleRegistroEntrada)
    {
        //
    }
}
