<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        return view('index');
    }
}
