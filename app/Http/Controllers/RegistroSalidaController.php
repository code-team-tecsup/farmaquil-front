<?php

namespace App\Http\Controllers;

use App\Disposicion;
use App\Documento;
use App\Existencia;
use App\Producto;
use App\Secuencia;
use App\Tipo;
use App\Usuario;
use App\Almacen;
use App\DetalleRegistroSalida;
use App\RegistroSalida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistroSalidaController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registrosalidas=RegistroSalida::all();
        return view('registroSalida.lista-registrosalida',compact('registrosalidas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacenes=Almacen::all();
        $usuarios=Usuario::all();
        $tipos=Tipo::all();
        $productos = Producto::all();
        $documentos=Documento::all();
        $disposiciones=Disposicion::all();
        $registrosalidas=RegistroSalida::all();

        return view('registroSalida.crear-registrosalida',[
            'almacenes'=>$almacenes,
            'usuarios'=>$usuarios,
            'tipos'=>$tipos,
            'documentos'=>$documentos,
            'productos' => $productos,
            'disposiciones' => $disposiciones,
            'registrosalidas' => $registrosalidas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //NO SE USA ESTE MÉTODO , SE USA DIRECTAMENTE EL COMPONENTE
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistroSalida  $RegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registrosalidas=RegistroSalida::find((int)$id);
        return view('registroSalida.eliminar-registrosalida', compact('registrosalidas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegistroSalida  $RegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacenes=Almacen::all();
        $usuarios=Usuario::all();
        $registrosalidas=RegistroSalida::find((int)$id);
        //dd($registrosalidas->detalleRegistroSalidas);
        return view('registroSalida.editar-registrosalida',['almacenes'=>$almacenes,'usuarios'=>$usuarios,
            'registrosalidas'=>$registrosalidas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistroSalida  $RegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistroSalida $RegistroSalida)
    {
        //NO SE REALIZAN ACTUALIZACIONES
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistroSalida  $RegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registrosalidasBuscado=RegistroSalida::find((int)$id);
        $listaDetalle=$registrosalidasBuscado->detalleRegistroEntradas;

        #RECOREMOS NUESTRO DETALLE
        foreach ($listaDetalle as $detalle){
            #SE BUSCA LA EXISTENCIA Y SE INCREMENTA SU STOCK
            $existencia = Existencia::where('idalmacen','=',$registrosalidasBuscado->idalmacen)->where('idproducto','=', $detalle->idproducto)->first();
            $existencia->stock=$existencia->stock + $detalle->cantidad;
            $existencia->update();
        }

        $registrosalidasBuscado->delete();

        return redirect()->route('registrosalidas.lista');
    }


}
