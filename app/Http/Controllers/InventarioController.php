<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\Existencia;
use App\Exports\InventarioExport;
use App\Exports\KardexExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class InventarioController extends Controller
{

    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        $almacenes=Almacen::all();
        return view('inventario.inventario',compact('almacenes'));
    }

    public function detalle($id)
    {
        $almacen=Almacen::find((int)$id);
        $existencias=Existencia::where('idalmacen','=',(int)$id)->orderBy('idproducto', 'asc')->get();

        return view('inventario.existencia',compact('almacen'),compact('existencias'));
    }

    public function exportar($id)
    {
        $almacen=Almacen::find((int)$id);
        $nombreArchivo="INVENTARIO_".$almacen->descripcion.".xlsx";
        $existencias=Existencia::where('idalmacen','=',(int)$id)->orderBy('idproducto', 'asc')->get();
        return Excel::download(new InventarioExport($existencias), $nombreArchivo);
    }

}
