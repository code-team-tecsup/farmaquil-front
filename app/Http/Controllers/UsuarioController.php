<?php

namespace App\Http\Controllers;

use App\Rol;
use App\Secuencia;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $usuarios = Usuario::paginate(10);
        return view('usuario.lista-usuario',compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $roles = Rol::all();
        return view('usuario.crear-usuario',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $apellidos = $request->get('txtApellidos');
        $nombres = $request->get('txtNombres');
        $nomusuario = $request->get('txtUsuario');
        $passwd = $request->get('txtPassword');
        $passwdConfirmado = $request->get('txtPasswordConfirmado');
        $rol = $request->get('txtRol');
        $activo = $request->get('txtActivo');

        #SI EXISTE UN PRODUCTO CON EL MISMO CODIGO ESTA DUPLICADO
        if($passwd!=$passwdConfirmado){
            return back()->withErrors('Las contraseñas no coindiden ');
        }
        #SI YA EXISTE U USUARIO ENTONCES ESTA DUPLICADO
        $usuarioBuscado=Usuario::where('usuario','=',$nomusuario)->first();
        if($usuarioBuscado!=null){
            return back()->withErrors('El usuario '.$nomusuario." ya EXISTE, ingrese un nombre de usuario diferente");
        }

        $usuario=new Usuario();
        $usuario->idusuario=Secuencia::nextid();
        $usuario->apellidos=$apellidos;
        $usuario->nombres=$nombres;
        $usuario->usuario=$nomusuario;
        $usuario->passwd=Hash::make($passwd);
        $usuario->idrol=(int)$rol;
        $usuario->activo = $activo;
        $usuario->save();

        return redirect()->route('usuarios.lista');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $usuario = Usuario::find((int)$id);
        return view('usuario.eliminar-usuario',compact('usuario' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $usuario = Usuario::find((int)$id);
        $roles = Rol::all();
        $act = $usuario->activo;
        return view('usuario.editar-usuario',compact('usuario' ),compact('roles') ,compact('act'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $apellidos = $request->get('txtApellidos');
        $nombres = $request->get('txtNombres');
        $nomusuario = $request->get('txtUsuario');
        $passwd = $request->get('txtPassword');
        $passwdConfirmado = $request->get('txtPasswordConfirmado');
        $rol = $request->get('txtRol');
        $activo = $request->get('txtActivo');

        #SI NO COINCIDEN LAS CONTRASEÑAS
        if($passwd!=$passwdConfirmado){
            return back()->withErrors('Las contraseñas no coindiden ');
        }

        $usuarioBuscado = Usuario::find((int)$id);
        $usuarioBuscado->apellidos = $apellidos;
        $usuarioBuscado->nombres = $nombres;
        $usuarioBuscado->usuario = $nomusuario;
        $usuarioBuscado->passwd=Hash::make($passwd);
        $usuarioBuscado->idrol = (int)$rol;
        $usuarioBuscado->activo = $activo;
        $usuarioBuscado->update();

        return redirect()->route('usuarios.lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usuario  $usuario
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ADMINISTRADOR']);

        $usuarioBuscado = Usuario::find((int)$id);
        $usuarioBuscado->delete();
        return redirect()->route('usuarios.lista');
    }


}
