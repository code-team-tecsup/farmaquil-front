<?php

namespace App\Http\Controllers;

use App\DetalleRegistroEntrada;
use App\DetalleRegistroSalida;
use App\Producto;
use App\RegistroEntrada;
use App\RegistroSalida;
use App\Tipo;
use App\Secuencia;
use App\UnidadMedida;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class ProductoController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $productos=Producto::paginate(10);
        return view('producto.lista-producto',compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $tipos=Tipo::all();
        $unidades=UnidadMedida::all();
        return view('producto.crear-producto',compact('tipos'),compact('unidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $codigo=$request->get('txtCodigo');
        $descripcion=$request->get('txtDescripcion');
        $presentacion=$request->get('txtPresentacion');
        $idunidad=$request->get('txtUnidad');
        $idtipo=$request->get('txtTipo');

        #SI EXISTE UN PRODUCTO CON EL MISMO CODIGO ESTA DUPLICADO
        $productoBuscado=Producto::find($codigo);
        if($productoBuscado!=null){
            return back()->withErrors('El código '.$codigo." ya EXISTE, ingrese un código diferente");
        }

        #SI YA EXISTE UN ALMACEN , SE ENCUENTRA DUPLICADO
        $productoBuscado2=Producto::where('descripcion','=',$descripcion)->first();
        if($productoBuscado2!=null){
            return back()->withErrors('El producto '.$descripcion." ya EXISTE, ingrese un nombre de producto diferente");
        }

        $producto=new Producto();
        $producto->idproducto=$codigo;
        $producto->descripcion=$descripcion;
        $producto->presentacion=$presentacion;
        $producto->idunidad=(int)$idunidad;
        $producto->idtipo=(int)$idtipo;
        $producto->save();

        return redirect()->route('productos.lista');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $producto=Producto::find($id);
        return view('producto.eliminar-producto',compact('producto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $tipos=Tipo::all();
        $unidades=UnidadMedida::all();
        $producto=Producto::find($id);

        #SI EXISTE UN PRODUCTO CON EL MISMO CODIGO ESTA DUPLICADO
        if($producto->atendido){
            return back()->withErrors('El ticket ya fue atendido, no se puede modificar');
        }


        return view('producto.editar-producto',['tipos'=>$tipos,'unidades'=>$unidades,'producto'=>$producto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $descripcion=$request->get('txtDescripcion');
        $concentracion=$request->get('txtConcentracion');
        $presentacion=$request->get('txtPresentacion');
        $idunidad=$request->get('txtUnidad');
        $idtipo=$request->get('txtTipo');

        $productoBuscado=Producto::find($id);
        $productoBuscado->descripcion=$descripcion;
        $productoBuscado->concentracion=$concentracion;
        $productoBuscado->presentacion=$presentacion;
        $productoBuscado->idtipo=(int)$idtipo;
        $productoBuscado->idunidad=(int)$idunidad;
        $productoBuscado->update();

        return redirect()->route('productos.lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $productoBuscado=Producto::find($id);

        $entradas=RegistroEntrada::where('detalleRegistroEntradas.idproducto','=',$id)->get();


        if(count($entradas)>0){
            return back()->withErrors('El producto '.$productoBuscado->descripcion." tiene ingresos, imposible eliminar");
        }

        $salidas=RegistroSalida::where('detalleRegistroSalidas.idproducto','=',$id)->get();
        if(count($salidas)>0){
            return back()->withErrors('El producto '.$productoBuscado->descripcion." tiene salidas, imposible eliminar");
        }

        $productoBuscado->delete();

        return redirect()->route('productos.lista');
    }


}
