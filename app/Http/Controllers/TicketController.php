<?php

namespace App\Http\Controllers;

use App\Notifications\TicketNotificacion;
use App\Secuencia;
use App\Ticket;
use App\Prioridad;
use App\User;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{

    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {

        $tickets=Ticket::where('idusuarioOrigen','=',Auth::user()->idusuario)->paginate(10);
        return view('ticket.lista-ticket',compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prioridades=Prioridad::all();
        $usuarios=Usuario::where('idusuario','<>',(int)Auth::user()->idusuario)->get();
        return view('ticket.crear-ticket',compact('prioridades'),compact('usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $fecha=$request->get('txtFecha');
        $titulo=$request->get('txtTitulo');
        $idprioridad=$request->get('txtPrioridad');
        $contenido=$request->get('txtContenido');
        $idusuario=$request->get('txtUsuario');

        $ticket=new Ticket();
        $ticket->fecha=$fecha;
        $ticket->idticket=Secuencia::nextid();
        $ticket->titulo=$titulo;
        $ticket->contenido=$contenido;
        $ticket->idprioridad=(int)$idprioridad;
        $ticket->idusuarioOrigen=Auth::user()->idusuario;
        $ticket->idusuarioDestino=(int)$idusuario;
        $ticket->atendido=false;

        $ticket->save();

        #$user=User::find((int)$idusuario);
        #$user->notify(new TicketNotificacion($ticket));

        return redirect()->route('tickets.lista');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ticket=Ticket::find((int)$id);

        return view('ticket.eliminar-ticket',compact('ticket'));
    }

    public function showTicketNotificado($idticket)
    {
        $ticket=Ticket::find((int)$idticket);

        return view('ticket.resolver-ticket',['ticket'=>$ticket]);
    }

    public function edit($id)
    {

        $prioridades=Prioridad::all();
        $usuarios=Usuario::where('idusuario','<>',(int)Auth::user()->idusuario)->get();

        $ticket=Ticket::find((int)$id);


        return view('ticket.editar-ticket',['ticket'=>$ticket,'prioridades'=>$prioridades,'usuarios'=>$usuarios ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $fecha=$request->get('txtFecha');
        $titulo=$request->get('txtTitulo');
        $idprioridad=$request->get('txtPrioridad');
        $contenido=$request->get('txtContenido');
        $idusuario=$request->get('txtUsuario');

        $ticketBuscado = Ticket::find((int)$id);
        $ticketBuscado->titulo=$titulo;
        $ticketBuscado->contenido=$contenido;
        $ticketBuscado->idprioridad=(int)$idprioridad;
        $ticketBuscado->idusuarioDestino=(int)$idusuario;
        $ticketBuscado->update();
        return redirect()->route('tickets.lista');
    }


    public function updateTicketNotificado(Request $request, $id)
    {
        $ticketBuscado = Ticket::find((int)$id);
        $ticketBuscado->atendido=true;
        $ticketBuscado->update();

        return redirect()->route('index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $ticketBuscado=Ticket::find((int)$id);
        $ticketBuscado->delete();

        return redirect()->route('tickets.lista');
    }
}
