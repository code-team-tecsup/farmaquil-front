<?php

namespace App\Http\Controllers;

use App\Almacen;
use App\RegistroEntrada;
use App\RegistroSalida;
use App\Tipo;
use App\Secuencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AlmacenController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacenes=Almacen::paginate(10);
        return view('almacen.lista-almacen',compact('almacenes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);
        $tipos=Tipo::all();
        return view('almacen.crear-almacen',compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $descripcion=$request->get('txtDescripcion');
        $idtipo=$request->get('txtTipo');

        #SI YA EXISTE UN ALMACEN , SE ENCUENTRA DUPLICADO
        $almacenBuscado=Almacen::where('descripcion','=',$descripcion)->first();
        if($almacenBuscado!=null){
            return back()->withErrors('El almacen '.$descripcion." ya EXISTE, ingrese un nombre de almacen diferente");
        }

        $almacen=new Almacen();
        $almacen->idalmacen=Secuencia::nextid();
        $almacen->descripcion=$descripcion;
        $almacen->idtipo=(int)$idtipo;
        $almacen->save();

        return redirect()->route('almacenes.lista');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacen=Almacen::find((int)$id);
        return view('almacen.eliminar-almacen',compact('almacen'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $tipos=Tipo::all();
        $almacen=Almacen::find((int)$id);
        return view('almacen.editar-almacen',compact('almacen'),compact('tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $descripcion=$request->get('txtDescripcion');
        $idtipo=$request->get('txtTipo');

        $almacenBuscado=Almacen::find((int)$id);
        $almacenBuscado->descripcion=$descripcion;
        $almacenBuscado->idtipo=(int)$idtipo;
        $almacenBuscado->update();

        return redirect()->route('almacenes.lista');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Almacen  $almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);
        $almacenBuscado=Almacen::find((int)$id);

        $entradas=RegistroEntrada::where('idalmacen','=',(int)$id)->get();
        if(count($entradas)>0){
            return back()->withErrors('El almacen '.$almacenBuscado->descripcion." tiene ingresos, imposible eliminar");
        }

        $salidas=RegistroSalida::where('idalmacen','=',(int)$id)->get();
        if(count($salidas)>0){
            return back()->withErrors('El almacen '.$almacenBuscado->descripcion." tiene salidas, imposible eliminar");
        }




        $almacenBuscado->delete();

        return redirect()->route('almacenes.lista');
    }


}
