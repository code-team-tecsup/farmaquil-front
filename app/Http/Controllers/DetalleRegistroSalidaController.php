<?php

namespace App\Http\Controllers;

use App\DetalleRegistroSalida;
use Illuminate\Http\Request;

class DetalleRegistroSalidaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleRegistroSalida  $detalleRegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleRegistroSalida $detalleRegistroSalida)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleRegistroSalida  $detalleRegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleRegistroSalida $detalleRegistroSalida)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleRegistroSalida  $detalleRegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleRegistroSalida $detalleRegistroSalida)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleRegistroSalida  $detalleRegistroSalida
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleRegistroSalida $detalleRegistroSalida)
    {
        //
    }
}
