<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    public function index()
    {
        return view('login.login');
    }

    public function logout(){
        Auth::logout();
        return view('login.login');
    }

    public function validar(Request $request)
    {
        $usuario=$request->get('txtUsuario');
        $password=$request->get('txtClave');

        $usuarioBuscado=Usuario::where('usuario','=',$usuario)->first();
        #SI SE EL USUARIO ES ENCONTRADO
        if($usuarioBuscado!=null){

            if (!Hash::check($password,$usuarioBuscado->passwd )){
                return back()->withErrors('Usuario o contraseña incorrecta');
            }else{
                if($usuarioBuscado->activo){
                    Auth::loginUsingId($usuarioBuscado->idusuario);
                    return view('index');
                }else{
                    return back()->withErrors('Usuario o contraseña incorrecta');
                }
            }
        }else{
            return back()->withErrors('Usuario o contraseña incorrecta');
        }
    }

}
