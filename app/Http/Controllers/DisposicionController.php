<?php

namespace App\Http\Controllers;

use App\Disposicion;
use Illuminate\Http\Request;

class DisposicionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Disposicion  $disposicion
     * @return \Illuminate\Http\Response
     */
    public function show(Disposicion $disposicion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Disposicion  $disposicion
     * @return \Illuminate\Http\Response
     */
    public function edit(Disposicion $disposicion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Disposicion  $disposicion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Disposicion $disposicion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Disposicion  $disposicion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Disposicion $disposicion)
    {
        //
    }
}
