<?php

namespace App\Http\Controllers;

use App\DetalleRegistroSalida;
use App\Disposicion;
use App\Documento;
use App\Existencia;
use App\Periodo;
use App\Producto;
use App\Secuencia;
use App\Tipo;
use App\Usuario;
use App\Almacen;
use App\DetalleRegistroEntrada;
use App\RegistroEntrada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegistroEntradaController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registroentradas=RegistroEntrada::all();

        return view('registroEntrada.lista-registroentrada',compact('registroentradas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacenes=Almacen::all();
        $usuarios=Usuario::all();
        $tipos=Tipo::all();
        $productos = Producto::all();
        $documentos=Documento::all();
        $disposiciones=Disposicion::all();
        $registroentradas=RegistroEntrada::all();
        $periodos=Periodo::all();

        return view('registroEntrada.crear-registroentrada',[
            'almacenes'=>$almacenes,
            'usuarios'=>$usuarios,
            'tipos'=>$tipos,
            'documentos'=>$documentos,
            'productos' => $productos,
            'disposiciones' => $disposiciones,
            'registroentradas' => $registroentradas,
            'periodos'=>$periodos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //NO SE USA ESTE MÉTODO , SE USA DIRECTAMENTE EL COMPONENTE
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RegistroEntrada  $registroEntrada
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registroentradas=RegistroEntrada::find((int)$id);
        return view('registroEntrada.eliminar-registroentrada', compact('registroentradas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RegistroEntrada  $registroEntrada
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $almacenes=Almacen::all();
        $usuarios=Usuario::all();
        $registroentradas=RegistroEntrada::find((int)$id);
        //dd($registroentradas->detalleRegistroEntradas);
        return view('registroEntrada.editar-registroentrada',['almacenes'=>$almacenes,'usuarios'=>$usuarios,
            'registroentradas'=>$registroentradas]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RegistroEntrada  $registroEntrada
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RegistroEntrada $registroEntrada)
    {
        //NO SE REALIZAN ACTUALIZACIONES
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RegistroEntrada  $registroEntrada
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        #SE VALIDA EL ROL AUTORIZADO
        Auth::user()->authorizeRoles(['ALMACEN']);

        $registroentradasBuscado=RegistroEntrada::find((int)$id);
        $listaDetalle=$registroentradasBuscado->detalleRegistroEntradas;

        #RECOREMOS NUESTRO DETALLE
        foreach ($listaDetalle as $detalle){
            #SE BUSCA LA EXISTENCIA Y SE DISMINUYE SU STOCK
            $existencia = Existencia::where('idalmacen','=',$registroentradasBuscado->idalmacen)->where('idproducto','=', $detalle->idproducto)->first();
            $existencia->stock=$existencia->stock-$detalle->cantidad;
            $existencia->update();
        }

        $registroentradasBuscado->delete();
        return redirect()->route('registroentradas.lista');
    }

}
