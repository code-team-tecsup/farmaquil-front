<?php

namespace App\Http\Controllers;

use App\Exports\KardexExport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Existencia;

class KardexController extends Controller
{
    public function __construct()
    {
        #VALIDAR QUE EL USUARIO ESTE AUTENTICADO
        $this->middleware('auth');
    }

    public function index()
    {
        return view('kardex.lista-reportekardex');
    }

    public function exportar()
    {
        $listaDetalle=Existencia::all();
        return Excel::download(new KardexExport($listaDetalle), 'Existencias.xlsx');
    }
}
