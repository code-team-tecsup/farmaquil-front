<?php

namespace App\Http\Livewire;

use App\Disposicion;
use App\Exports\KardexExport;
use App\Http\Controllers\KardexController;
use App\Tipo;
use App\Almacen;
use App\Producto;
use App\Periodo;
use App\DtoDetalleIngreso;
use App\RegistroEntrada;
use GuzzleHttp\Client;
use Livewire\Component;
use Maatwebsite\Excel\Facades\Excel;

class Kardex extends Component
{
    #PROPIEDADES PARA LLENAR LOS SELECTS
    public $listaTipo;
    public $listaAlmacen;
    public $listaProducto;
    public $listaPeriodo;

    #DETALLE DEL INGRESO
    public $listaDetalle;

    #PROPIEDADES PARA ALMACENAR LOS VALORES DEL CONTROLES DE LOS FORMULARIOS
    #CONTROLES DE CABECERA
    public $idtipo;
    public $idalmacen;
    public $idproducto;
    public $idperiodo;

    #PROPIEDADES PARA DESHABILITAR LOS CONTROLES
    public $activo;
    public $procesar;


    public function filtro($idtipo): void
    {
        $this->idtipo = $idtipo;
        $this->listaAlmacen=Almacen::where('idtipo','=',(int)$idtipo)->get();
        $this->listaProducto=Producto::where('idtipo','=',(int)$idtipo)->get();

        $this->idalmacen=$this->listaAlmacen[0]->idalmacen;
        $this->idproducto=$this->listaProducto[0]->idproducto;

    }

    public function render()
    {
        return view('livewire.kardex');
    }

    public function verDetalle($item)
    {
        $detalle=$this->listaDetalle[$item-1];

        if($detalle['tipo']=="ENTRADA"){
           redirect()->action('RegistroEntradaController@edit',$detalle['id']);

        }else{
            redirect()->action('RegistroSalidaController@edit',$detalle['id']);
        }

    }


    public function mount()
    {
        $this->listaTipo = Tipo::all();
        $this->listaAlmacen=[];
        $this->listaProducto=[];
        $this->filtro($this->listaTipo[0]->idtipo);
        $this->listaDisposicion = Disposicion::all();
        $this->listaPeriodo = Periodo::all();

        $this->idperiodo=$this->listaPeriodo[6]->idperiodo;
        $this->iddisposicion=$this->listaDisposicion[2]->iddisposicion;

        $this->listaDetalle=array();

        $this->activo=true;
        $this->procesar="";
    }

    public function procesoKardex()
    {
        $this->validate([
            'idalmacen' => 'required',
            'idproducto' => 'required',
            'idperiodo' => 'required',
        ]);

        try {
            $client = new Client(['base_uri' => 'http://localhost:7000/farmaquil/']);
            $response = $client->request('GET', 'kardex/'. $this->idalmacen.'/'.$this->idperiodo.'/'.$this->idproducto);
            $statusCode = $response->getStatusCode();

            if($statusCode==200){
                $body = $response->getBody()->getContents();
                $this->listaDetalle=array();
                $listaTemporal=(array)json_decode($body);
                foreach ($listaTemporal as $detalle){
                    array_push($this->listaDetalle,(array)$detalle);
                }
            }
        }catch (\Exception $e){
            $this->validate([
                'procesar' => 'required'
            ]);
        }
        #SI EL USUARIO PROESA UN PRODUCTO
        #NO SE DEBE PERMITIR QUE CAMBIE DE ALMACEN
        $this->activo=false;
    }

    public function limpiarKardex()
    {

        $this->listaDetalle=array();
        $this->activo=true;
    }

    public function exportar()
    {
        #dd(Excel::download(new KardexExport($this->listaDetalle), 'Existencias.xlsx'));
        return Excel::download(new KardexExport($this->listaDetalle), 'Existencias.xlsx');
        #return redirect()->action('KardexController@exportar',$this->listaDetalle);
    }

}
