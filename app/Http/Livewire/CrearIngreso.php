<?php

namespace App\Http\Livewire;

use App\Almacen;
use App\DetalleRegistroEntrada;
use App\Disposicion;
use App\Documento;
use App\DtoDetalleIngreso;
use App\Existencia;
use App\Periodo;
use App\Producto;
use App\RegistroEntrada;
use App\Secuencia;
use App\Tipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class CrearIngreso extends Component
{
    #PROPIEDADES PARA LLENAR LOS SELECTS
    public $listaTipo;
    public $listaAlmacen;
    public $listaDocumento;
    public $listaProducto;
    public $listaPeriodo;
    public $listaDisposicion;

    #DETALLE DEL INGRESO
    public $listaDetalle;

    #PROPIEDADES PARA ALMACENAR LOS VALORES DEL CONTROLES DE LOS FORMULARIOS
    #CONTROLES DE CABECERA
    public $idalmacen;
    public $idtipo;
    public $fecha;
    public $iddocumento;
    public $serie;
    public $numero;

    #CONTROLES EXCLUSIVOS PARA AGREGAR UN NUEVO DETALLE
    public $idproducto;
    public $iddisposicion;
    public $idperiodo;
    public $cantidad;
    public $lote;
    public $idItem;

    #PROPIEDADES PARA DESHABILITAR LOS CONTROLES
    public $activo;


    public function filtro($idtipo): void
    {
        $this->idtipo = $idtipo;
        $this->listaAlmacen=Almacen::where('idtipo','=',(int)$idtipo)->get();
        $this->listaProducto=Producto::where('idtipo','=',(int)$idtipo)->get();

        $this->idalmacen=$this->listaAlmacen[0]->idalmacen;
        $this->idproducto=$this->listaProducto[0]->idproducto;

    }

    public function agregarDetalle()
    {
        #SE VALIDA QUE EL USUARIO INGRESEN VALORES EN LOS CONTROLES
        $this->validate([
            'idalmacen' => 'required',
            'idproducto' => 'required',
            'cantidad' => 'required|integer|min:1',
            'lote' => 'required',
        ]);


        $producto=Producto::find($this->idproducto);
        $periodo=Periodo::find((int)$this->idperiodo);
        $disposicion=Disposicion::find((int)$this->iddisposicion);
        #SE CREA UN OBJETO DTODETALLEINGRESO Y LE ASIGNAMOS LAS PROPIEDADES QUE CORRESPONDEN
        $detalle=new DtoDetalleIngreso();
        $detalle->iddetalle=$this->idItem;
        $detalle->idproducto=$producto->idproducto;
        $detalle->descripcionProducto=$producto->descripcion;
        $detalle->iddisposicion=$disposicion->iddisposicion;
        $detalle->descripcionDisposicion=$disposicion->descripcion;
        $detalle->cantidad=$this->cantidad;
        $detalle->lote=$this->lote;
        $detalle->vencimiento=$periodo->descripcion;

        $this->listaDetalle[$this->idItem]=(array)$detalle;

        #SI EL USUARIO AGREGA UN PRODUCTO AL DETALLE
        #NO SE DEBE PERMITIR QUE CAMBIE DE ALMACEN
        $this->activo=false;
        #SE LIMPIAN LOS CONTROLES
        $this->lote="";
        $this->cantidad="";

        #INCREMENTAMOS LOS ITEMS INGRESADOS
        $this->idItem++;
    }

    public function eliminarDetalle($iddetalle)
    {
        unset($this->listaDetalle[$iddetalle]);
    }

    public function mount()
    {
        $this->listaTipo = Tipo::all();
        $this->listaDocumento = Documento::all();
        $this->listaAlmacen=[];
        $this->listaProducto=[];
        $this->filtro($this->listaTipo[0]->idtipo);
        $this->listaDisposicion = Disposicion::all();
        $this->listaPeriodo = Periodo::all();

        $this->iddocumento=$this->listaDocumento[0]->iddocumento;
        $this->idperiodo=$this->listaPeriodo[0]->idperiodo;
        $this->iddisposicion=$this->listaDisposicion[2]->iddisposicion;

        $this->listaDetalle=array();

        $this->fecha=date('Y-m-d', time());
        $this->activo=true;
        $this->idItem=0;
    }

    public function render()
    {
        return view('livewire.crear-ingreso');
    }

    public function store()
    {
        #SE VALIDA QUE EL USUARIO INGRESEN VALORES EN LOS CONTROLES
        $this->validate([
            'idalmacen' => 'required',
            'fecha' => 'required',
            'serie' => 'required',
            'numero' => 'required',
            'listaDetalle' => 'required'
        ]);

        try {

            $registroentrada=new RegistroEntrada();
            $registroentrada->identrada=Secuencia::nextid();
            $registroentrada->idalmacen=(int)$this->idalmacen;
            $registroentrada->fecha=$this->fecha;
            $registroentrada->iddocumento=$this->iddocumento;
            $registroentrada->serie=$this->serie;
            $registroentrada->numero=$this->numero;
            $registroentrada->idusuario=Auth::user()->idusuario;
            $registroentrada->save();

            #RECOREMOS NUESTRO DETALLE
            foreach ($this->listaDetalle as $detalle){
                $detalleRegistroEntrada=new DetalleRegistroEntrada();
                $detalleRegistroEntrada->iddetalle=Secuencia::nextid();
                $detalleRegistroEntrada->idproducto=$detalle['idproducto'];
                $detalleRegistroEntrada->iddisposicion=$detalle['iddisposicion'];
                $detalleRegistroEntrada->cantidad=$detalle['cantidad'];
                $detalleRegistroEntrada->lote=$detalle['lote'];
                $detalleRegistroEntrada->vencimiento=$detalle['vencimiento'];
                $registroentrada->detalleRegistroEntradas()->save($detalleRegistroEntrada);

                #SE BUSCA LA EXISTENCIA Y SE INCREMENTA SU STOCK
                $existencia = Existencia::where('idalmacen','=',$registroentrada->idalmacen)->where('idproducto','=', $detalleRegistroEntrada->idproducto)->first();
                $existencia->stock=$existencia->stock +$detalleRegistroEntrada->cantidad;
                $existencia->update();
            }

        }catch (\Exception $e){

        }

        return redirect()->route('registroentradas.lista');
    }
}
