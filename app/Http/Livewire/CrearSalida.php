<?php

namespace App\Http\Livewire;

use App\Almacen;
use App\DetalleRegistroSalida;
use App\Disposicion;
use App\Documento;
use App\DtoDetalleSalida;
use App\Existencia;
use App\Periodo;
use App\Producto;
use App\RegistroSalida;
use App\Secuencia;
use App\Tipo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class CrearSalida extends Component
{
    #PROPIEDADES PARA LLENAR LOS SELECTS
    public $listaTipo;
    public $listaAlmacen;
    public $listaDocumento;
    public $listaExistencia;
    public $listaPeriodo;

    #DETALLE DEL Salida
    public $listaDetalle;

    #PROPIEDADES PARA ALMACENAR LOS VALORES DEL CONTROLES DE LOS FORMULARIOS
    #CONTROLES DE CABECERA
    public $idalmacen;
    public $idtipo;
    public $fecha;
    public $iddocumento;
    public $serie;
    public $numero;

    #CONTROLES EXCLUSIVOS PARA AGREGAR UN NUEVO DETALLE
    public $idproducto;
    public $iddisposicion;
    public $idperiodo;
    public $cantidad;
    public $lote;
    public $idItem;

    #PROPIEDADES PARA DESHABILITAR LOS CONTROLES
    public $activo;


    public function filtroTipo($idtipo): void
    {
        $this->idtipo = $idtipo;
        $this->listaAlmacen=Almacen::where('idtipo','=',(int)$idtipo)->get();
        $this->idalmacen=$this->listaAlmacen[0]->idalmacen;

        $this->filtroAlmacen($this->idalmacen);

    }

    public function filtroAlmacen($idalmacen): void
    {
        $this->listaExistencia=Existencia::where('idalmacen','=',(int)$idalmacen)->get();
        $this->idproducto=$this->listaExistencia[0]->producto->idproducto;
    }

    public function agregarDetalle()
    {


        $existencia=Existencia::where('idalmacen','=',$this->idalmacen)->where('idproducto','=',$this->idproducto)->first();
        $maximo='max:'.(int)$existencia->stock;


        #SE VALIDA QUE EL USUARIO INGRESEN VALORES EN LOS CONTROLES
        $this->validate([
            'idalmacen' => 'required',
            'idproducto' => 'required',
            'cantidad' => 'required|integer|min:1|'.$maximo,
            'lote' => 'required',
        ]);




        $producto=Producto::find($this->idproducto);
        $periodo=Periodo::find((int)$this->idperiodo);
        #SE CREA UN OBJETO DTODETALLESALIDA Y LE ASIGNAMOS LAS PROPIEDADES QUE CORRESPONDEN
        $detalle=new DtoDetalleSalida();
        $detalle->iddetalle=$this->idItem;
        $detalle->idproducto=$producto->idproducto;
        $detalle->descripcionProducto=$producto->descripcion;
        $detalle->iddisposicion=$this->iddisposicion;
        $detalle->cantidad=$this->cantidad;
        $detalle->lote=$this->lote;
        $detalle->vencimiento=$periodo->descripcion;

        $this->listaDetalle[$this->idItem]=(array)$detalle;

        #SI EL USUARIO AGREGA UN PRODUCTO AL DETALLE
        #NO SE DEBE PERMITIR QUE CAMBIE DE ALMACEN
        $this->activo=false;
        #SE LIMPIAN LOS CONTROLES
        $this->lote="";
        $this->cantidad="";

        #INCREMENTAMOS LOS ITEMS INGRESADOS
        $this->idItem++;
    }

    public function eliminarDetalle($iddetalle)
    {
        unset($this->listaDetalle[$iddetalle]);
    }

    public function mount()
    {
        $this->listaTipo = Tipo::all();
        $this->listaDocumento = Documento::all();
        $this->listaAlmacen=[];
        $this->listaExistencia=[];
        $this->filtroTipo($this->listaTipo[0]->idtipo);
        $this->listaDisposicion = Disposicion::all();
        $this->listaPeriodo = Periodo::all();

        $this->iddocumento=$this->listaDocumento[0]->iddocumento;
        $this->idperiodo=$this->listaPeriodo[0]->idperiodo;
        $this->iddisposicion=$this->listaDisposicion[2]->iddisposicion;
        $this->listaDetalle=array();

        $this->fecha=date('Y-m-d', time());
        $this->activo=true;
        $this->idItem=0;
    }

    public function render()
    {
        return view('livewire.crear-salida');
    }

    public function store()
    {


        #SE VALIDA QUE EL USUARIO INGRESEN VALORES EN LOS CONTROLES
        $this->validate([
            'idalmacen' => 'required',
            'fecha' => 'required',
            'serie' => 'required',
            'numero' => 'required',
            'listaDetalle' => 'required'
        ]);

        try {
            $registroSalida=new RegistroSalida();
            $registroSalida->idsalida=Secuencia::nextid();
            $registroSalida->idalmacen=(int)$this->idalmacen;
            $registroSalida->fecha=$this->fecha;
            $registroSalida->iddocumento=$this->iddocumento;
            $registroSalida->serie=$this->serie;
            $registroSalida->numero=$this->numero;
            $registroSalida->idusuario=Auth::user()->idusuario;
            $registroSalida->save();

            #RECOREMOS NUESTRO DETALLE
            foreach ($this->listaDetalle as $detalle){
                $detalleRegistroSalida=new DetalleRegistroSalida();
                $detalleRegistroSalida->iddetalle=Secuencia::nextid();
                $detalleRegistroSalida->idproducto=$detalle['idproducto'];
                $detalleRegistroSalida->iddisposicion=  $detalle['iddisposicion'];
                $detalleRegistroSalida->cantidad=$detalle['cantidad'];
                $detalleRegistroSalida->lote=$detalle['lote'];
                $detalleRegistroSalida->vencimiento=$detalle['vencimiento'];
                $registroSalida->detalleRegistroSalidas()->save($detalleRegistroSalida);

                #SE BUSCA LA EXISTENCIA Y SE DISMINUYE SU STOCK
                $existencia = Existencia::where('idalmacen','=',$registroSalida->idalmacen)->where('idproducto','=', $detalleRegistroSalida->idproducto)->first();
                $existencia->stock=$existencia->stock - $detalleRegistroSalida->cantidad;
                $existencia->update();
            }

        }catch (\Exception $e){

        }

        return redirect()->route('registrosalidas.lista');
    }
}
