<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Usuario extends Model
{
    protected $primaryKey= 'idusuario';
    public function rol(){
        return $this->hasOne('App\Rol','idrol','idrol');
    }
}
