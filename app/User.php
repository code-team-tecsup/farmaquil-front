<?php

namespace App;

use Boytunghc\LaravelMongoNotifiable\DatabaseNotification;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\MustVerifyEmail;
#use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Mongodb\Auth\User as Authenticatable;
#use Illuminate\Notifications\Notifiable;


class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'usuarios';
    protected $primaryKey= 'idusuario';

    public function ticketPrioridad(){

        $listaRetorno=[];

        try {
            $client = new Client(['base_uri' => 'http://localhost:7000/farmaquil/']);
            $response = $client->request('GET', 'prioridad/'.Auth::user()->idusuario);
            $statusCode = $response->getStatusCode();

            if($statusCode==200){
                $body = $response->getBody()->getContents();
                $this->listaDetalle=array();
                $listaTemporal=(array)json_decode($body);

                foreach ($listaTemporal as $detalle){
                    array_push($listaRetorno,(array)$detalle);
                }
            }
        }catch (\Exception $e){
            $listaRetorno=[];
        }

        return $listaRetorno;
    }

    public function rol(){
        return $this->hasOne('App\Rol','idrol','idrol');
    }

    public function authorizeRoles($roles)
    {
        abort_unless($this->hasAnyRole($roles), 401);
        return true;
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->rol->descripcion==$role) {
            return true;
        }
        return false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
