<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Producto extends Model
{
    protected $primaryKey = 'idproducto';

    public function tipo(){
        return $this->belongsTo('App\Tipo','idtipo','idtipo');
    }

    public function unidadMedida(){
        return $this->belongsTo('App\UnidadMedida','idunidad','idunidad');
    }
}
