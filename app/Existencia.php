<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Existencia extends Model
{
    protected $primaryKey = 'idexistencia';

    public function producto(){
        return $this->belongsTo('App\Producto','idproducto','idproducto');
    }

    public function almacen(){
        return $this->belongsTo('App\Almacen','idalmacen','idalmacen');
    }
}
