<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Ticket extends Model
{

    protected $primaryKey = 'idticket';

    public function prioridad(){
        return $this->belongsTo('App\Prioridad','idprioridad','idprioridad');
    }

    public function usuarioOrigen(){
        return $this->belongsTo('App\Usuario','idusuarioOrigen','idusuario');
    }

    public function usuarioDestino(){
        return $this->belongsTo('App\Usuario','idusuarioDestino','idusuario');
    }
}
