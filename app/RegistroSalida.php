<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class RegistroSalida extends Model
{
    protected $primaryKey = 'idsalida';

    public function almacen(){
        return $this->belongsTo('App\Almacen','idalmacen','idalmacen');
    }

    public function documento(){
        return $this->belongsTo('App\Documento','iddocumento','iddocumento');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario','idusuario','idusuario');
    }

    #UN REGISTRO DE SALIDAS TIENE MUCHOS PRODUCTOS
    public function detalleRegistroSalidas()
    {
        return $this->embedsMany('App\DetalleRegistroSalida');
    }

}
