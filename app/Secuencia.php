<?php


namespace App;
use Illuminate\Support\Facades\DB;
use MongoDB\Operation\FindOneAndUpdate;


class Secuencia
{

    public static function nextid()
    {
        // ref is the counter - change it to whatever you want to increment
        return self::getID();
    }


    private static function getID()
    {
        $seq = DB::connection('mongodb')->getCollection('counters')->findOneAndUpdate(
            ['ref' => 'ref'],
            ['$inc' => ['seq' => 1]],
            ['new' => true, 'upsert' => true, 'returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER]
        );
        return $seq->seq;
    }

}
