<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class DetalleRegistroSalida extends Model
{
    protected $primaryKey = 'iddetalle';

    public function producto(){
        return $this->belongsTo('App\Producto','idproducto','idproducto');
    }

    public function disposicion(){
        return $this->belongsTo('App\Disposicion','iddisposicion','iddisposicion');
    }
}
