<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Prioridad extends Model
{
    protected $primaryKey = 'idprioridad';
}
