<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Almacen extends Model
{

    protected $primaryKey = 'idalmacen';

    public function tipo(){
        return $this->belongsTo('App\Tipo','idtipo','idtipo');
    }
}
