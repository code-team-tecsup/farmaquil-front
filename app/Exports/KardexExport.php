<?php

namespace App\Exports;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class KardexExport implements FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */

    /**
     * KardexExport constructor.
     * @param $listaDetalle
     */

    public $listaDetalle;

    public function __construct($listaDetalle)
    {
        $this->listaDetalle = $listaDetalle;
    }

    public function collection()
    {
        return $this->listaDetalle;
    }


    public function map($data): array
    {
        $this->headings();

        return [
                $data->idproducto,
                $data->producto->descripcion,
                $data->stock
        ];
    }

    public function headings(): array
    {
        return [
            ['CODIGO','DESCRIPCION', 'STOCK']
        ];
    }


    public function registerEvents(): array
    {

        return [
            AfterSheet::class=> function(AfterSheet $event) {
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                    'fill' => [
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                        'color' => ['rgb' => "CCCCCC"]
                    ]
                ];

                $cellRange = 'A1:C1';
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray($styleArray);
            },
        ];
    }



}
