<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class RegistroEntrada extends Model
{
    protected $primaryKey = 'identrada';

    public function almacen(){
        return $this->belongsTo('App\Almacen','idalmacen','idalmacen');
    }

    public function documento(){
        return $this->belongsTo('App\Documento','iddocumento','iddocumento');
    }

    public function usuario(){
        return $this->belongsTo('App\Usuario','idusuario','idusuario');
    }

    #UN REGISTRO DE ENTRADA TIENE MUCHOS PRODUCTOS
    public function detalleRegistroEntradas()
    {
        return $this->embedsMany('App\DetalleRegistroEntrada');
    }

}
