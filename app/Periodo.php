<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Periodo extends Model
{
    protected $primaryKey = 'idperiodo';
}
