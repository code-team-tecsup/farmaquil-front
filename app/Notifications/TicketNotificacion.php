<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TicketNotificacion extends Notification
{

    protected $ticket;

    public function __construct($ticket)
    {
        $this->ticket=$ticket;
    }

    public function via($notifiable)
    {
        return [ 'database'];
    }

    #IMPLEMENTAMOS LA VIA
    public function toDatabase($notifiable)
    {
        #ALMACENAMOS EL ID DEL POST, SU TITULO Y EL NOMBRE DEL USUARIO QUE COMENTO
        return [
            'ticket_idticket' => $this->ticket->idticket,
            'ticket_titulo' => $this->ticket->titulo,
            'ticket_usuarioOrigen' =>  $this->ticket->usuarioOrigen->nombres
        ];
    }
    public function toArray($notifiable)
    {
        return [ ];
    }



}
