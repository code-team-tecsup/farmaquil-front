<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\RegistroSalida;
use Faker\Generator as Faker;

$factory->define(RegistroSalida::class, function (Faker $faker) {
    return [
        'idsalida' => $faker->numberBetween(1,1000),
        'idalmacen' => App\Almacen::all(['idalmacen'])->random(),
        'iddocumento' => App\Documento::all(['iddocumento'])->random(),
        'idusuario' => App\Usuario::all(['idusuario'])->random(),
        'serie'=>"F100",
        'numero'=>strval($faker->randomNumber(6)),
        'fecha'=>$faker->date($format = 'Y-m-d', $min = 'now'),
    ];
});
