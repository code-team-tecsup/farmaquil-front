<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Disposicion;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Disposicion::class, function (Faker $faker) {
    return [
        'iddisposicion' => $faker->numberBetween(1,1000),
        'descripcion'=>$faker->word()
    ];
});
