<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Producto;
use Faker\Generator as Faker;

$factory->define(Producto::class, function (Faker $faker) {
    return [
        'idproducto'=>$faker->word(),#se requiere un codigo de 6 digitos
        'idtipo' => App\Tipo::all(['idtipo'])->random(),
        'idunidad' => App\UnidadMedida::all(['idunidad'])->random(),
        'descripcion'=>$faker->sentence(3),
        'presentacion'=>$faker->sentence(4),
    ];
});
