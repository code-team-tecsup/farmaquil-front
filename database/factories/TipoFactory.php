<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Tipo;
use Faker\Generator as Faker;

$factory->define(Tipo::class, function (Faker $faker) {
    return [
        'idtipo' => $faker->numberBetween(1,1000),
        'descripcion'=>$faker->word()
    ];
});
