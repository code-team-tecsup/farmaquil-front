<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Existencia;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Existencia::class, function (Faker $faker) {
    return [
        'idexistencia' => $faker->numberBetween(1,1000),
        'idproducto' => App\Producto::all(['idproducto'])->random(),
        'idalmacen' => App\Almacen::all(['idalmacen'])->random(),
        'stock'=>$faker->randomNumber(3),
    ];
});
