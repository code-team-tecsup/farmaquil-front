<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Almacen;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Almacen::class, function (Faker $faker) {
    return [
        'idalmacen' => $faker->numberBetween(1,1000),
        'descripcion'=>$faker->sentence(3),
        'idtipo' => App\Tipo::all(['idtipo'])->random(),
    ];
});
