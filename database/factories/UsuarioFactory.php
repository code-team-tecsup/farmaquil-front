<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Usuario;
use Faker\Generator as Faker;

$factory->define(Usuario::class, function (Faker $faker) {
    #SE ASIGNA LA MISMA CONTRASEÑA A TODOS LOS USUARIOS
    $hashed_password = Hash::make('123456');
    return [
        'idusuario' => $faker->numberBetween(1,1000),
        'apellidos' => $faker->firstName,
        'nombres' => $faker->name,
        'passwd' => $hashed_password,
        'activo'=>true,
        'idrol' => App\Rol::all(['idrol'])->random(),
    ];
});
