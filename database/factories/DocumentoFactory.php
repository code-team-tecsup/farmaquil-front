<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Documento;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Documento::class, function (Faker $faker) {
    return [
        'iddocumento' => strval($faker->randomNumber(2)),
        'descripcion'=>$faker->word()
    ];
});
