<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DetalleRegistroEntrada;
use App\Model;
use Faker\Generator as Faker;

$factory->define(DetalleRegistroEntrada::class, function (Faker $faker) {
    return [
        'iddetalle' => $faker->numberBetween(1,1000),
        'idproducto' => App\Producto::all(['idproducto'])->random(),
        'iddisposicion' => App\Disposicion::all(['iddisposicion'])->random(),
        'cantidad'=>$faker->randomNumber(3),
        'lote'=>$faker->word(),
        'vencimiento'=>$faker->date($format = 'Y-m', $min = 'now'),
    ];
});
