<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\UnidadMedida;
use Faker\Generator as Faker;

$factory->define(UnidadMedida::class, function (Faker $faker) {
    return [
        'idunidad' => $faker->numberBetween(1,1000),
        'descripcion'=>$faker->word()
    ];
});
