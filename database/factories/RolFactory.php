<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Rol;
use Faker\Generator as Faker;

$factory->define(Rol::class, function (Faker $faker) {
    return [
        'idrol' => $faker->numberBetween(1,1000),
        'descripcion'=>$faker->word()
    ];
});
