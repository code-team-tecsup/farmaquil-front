<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\RegistroEntrada;
use Faker\Generator as Faker;

$factory->define(RegistroEntrada::class, function (Faker $faker) {
    return [
        'identrada' => $faker->numberBetween(1,1000),
        'idalmacen' => App\Almacen::all(['idalmacen'])->random(),
        'iddocumento' => App\Documento::all(['iddocumento'])->random(),
        'idusuario' => App\Usuario::all(['idusuario'])->random(),
        'serie'=>"F001",
        'numero'=>strval($faker->randomNumber(6)),
        'fecha'=>$faker->date($format = 'Y-m-d', $min = 'now'),
    ];
});
