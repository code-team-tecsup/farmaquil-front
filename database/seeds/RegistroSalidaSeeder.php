<?php

use Illuminate\Database\Seeder;

class RegistroSalidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $salidas=factory(\App\RegistroSalida::class,5)->create()->each(function ($registroentrada){

            $registroentrada->detalleRegistroSalidas()->createMany(
                factory(\App\DetalleRegistroSalida::class,3)->make()->toArray()
            );

        });
    }
}
