<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        #USUARIO
        $this->call(RolSeeder::class);
        $this->call(UsuarioSeeder::class);

        #ALMACEN
        $this->call(TipoSeeder::class);
        $this->call(AlmacenSeeder::class);

        #PRODUCTO
        $this->call(UnidadMedidaSeeder::class);
        $this->call(ProductoSeeder::class);

        #EXISTENCIA
        $this->call(ExistenciaSeeder::class);

        #DOCUMENTO
        $this->call(DocumentoSeeder::class);

        #DISPOSICION
        $this->call(DisposicionSeeder::class);

        #REGISTRO DE ENTRADAS
        $this->call(RegistroEntradaSeeder::class);



        #REGISTRO DE SALIDA
        $this->call(RegistroSalidaSeeder::class);




    }
}

