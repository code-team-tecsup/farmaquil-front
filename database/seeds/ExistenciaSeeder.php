<?php

use Illuminate\Database\Seeder;

class ExistenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Existencia::class,10)->create();
    }
}
