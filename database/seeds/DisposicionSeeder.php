<?php

use Illuminate\Database\Seeder;

class DisposicionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Disposicion::class,3)->create();
    }
}
