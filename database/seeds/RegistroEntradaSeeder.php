<?php

use Illuminate\Database\Seeder;

class RegistroEntradaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entradas=factory(\App\RegistroEntrada::class,5)->create()->each(function ($registroentrada){

            $registroentrada->detalleRegistroEntradas()->createMany(
                factory(\App\DetalleRegistroEntrada::class,3)->make()->toArray()
            );

        });

    }
}
